package com.github.dnay8979;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    // 2D array consisting of 3 rows and 3 columns
    private Button[][] grid = new Button[3][3];

    private boolean playerXTurn = true;
    private boolean playerXWins = false;
    private boolean playerOWins = false;
    private boolean tieGame = false;
    private boolean endOfGame = false;
    private int turnCounter;
    private TextView gameStatusTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Get references for TextViews
        gameStatusTextView = findViewById(R.id.gameStatusTextView);

        // Assign grid/array by referencing the boxes in a nested loop
        for (int i = 0; i < 3; i++){
            for (int j = 0; j < 3; j++){
                // Pass ResourceID for findViewById
                String boxID = "box" + i + j + "Button";
                int idToLookFor = getResources().getIdentifier(boxID,"id", getPackageName());
                grid[i][j] = findViewById(idToLookFor);
                grid[i][j].setOnClickListener(this); // pass main activity (this) as onClickListener
            }
        }
        // Handle 'New Game' button click separately
        Button buttonNewGame = findViewById(R.id.newGameButton);
        buttonNewGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newGame();
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (!((Button) v).getText().toString().equals("")){
            return;
        }

        if (!endOfGame){
            if (playerXTurn) {
                ((Button) v).setText("X");
            } else {
                ((Button) v).setText("O");
            }
        }

        turnCounter++;

        if (checkForWinner() && playerXTurn){
            playerXWins = true;
        } else if (checkForWinner() && !playerXTurn){
            playerOWins = true;
        } else if (turnCounter == 9){
            tieGame = true;
        } else{
            playerXTurn = !playerXTurn;
        }
        updateGameStatus();
    }

    private boolean checkForWinner() {
        // Convert grid/button array into a string array for comparison
        String[][] index = new String[3][3];

        for (int i = 0; i < 3; i++){
            for (int j = 0; j < 3; j++){
                index[i][j] = grid[i][j].getText().toString();
            }
        }

        // Check win conditions in columns or rows
        for (int i = 0; i < 3; i++) {
            if ((index[i][0].equals(index[i][1])
                    && index[i][0].equals(index[i][2])
                    && !index[i][0].equals(""))
                    || (index[0][i].equals(index[1][i])
                    && index[0][i].equals(index[2][i])
                    && !index[0][i].equals(""))){
                endOfGame = true;
                return true;
            }
        }

        // Check diagonal win conditions
        if ((index[0][0].equals(index[1][1])
                && index[0][0].equals(index[2][2])
                && !index[0][0].equals(""))
                || (index[0][2].equals(index[1][1])
                && index[0][2].equals(index[2][0])
                && !index[0][2].equals(""))){
            endOfGame = true;
            return true;
        }
        return false;
    }

    private void updateGameStatus(){
        if (playerXWins){
            gameStatusTextView.setText("Player X Wins");
        } else if (playerOWins){
            gameStatusTextView.setText("Player O Wins");
        } else if (tieGame) {
            gameStatusTextView.setText("Tie Game");
        } else if (playerXTurn){
            gameStatusTextView.setText("Player X's Turn");
        } else if (!playerXTurn){
            gameStatusTextView.setText("Player O's Turn");
        } else {
            gameStatusTextView.setText("");
        }
    }

    // Clear the board and reset game
    private void newGame(){
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                grid[i][j].setText("");
            }
        }
        playerXTurn = true;
        playerXWins = false;
        playerOWins = false;
        tieGame = false;
        endOfGame = false;
        turnCounter = 0;

        updateGameStatus();
    }

    // Maintain game state when switching orientation or apps
    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState){
        super.onSaveInstanceState(savedInstanceState);

        savedInstanceState.putBoolean("playerXTurn", playerXTurn);
        savedInstanceState.putBoolean("endOfGame", endOfGame);
        savedInstanceState.putInt("turnCounter", turnCounter);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        playerXTurn = savedInstanceState.getBoolean("playerXTurn");
        endOfGame = savedInstanceState.getBoolean("endOfGame");
        turnCounter = savedInstanceState.getInt("turnCounter");
    }
}
