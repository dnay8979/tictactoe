# TicTacToe

A simple tic tac toe game built with Java on Android Studio :-)

![TicTacToePreview](https://bitbucket.org/dnay8979/tictactoe/raw/aa5866ef07e29bbefd4fb7fb229eb613aebf84e8/TicTacToePreview.png)

## Installation

Windows: Android Studio is required to run the project. 

* Download a copy of the project repo from [here](https://bitbucket.org/dnay8979/tictactoe/downloads/)

* Alternatively, you can run the command `git clone https://dnay8979@bitbucket.org/dnay8979/tictactoe.git` in a git terminal

* Open the project in Android Studio and run it using a virtual device (e.g. Pixel 2 API 29)

## How to Play

* Player "X" goes first. 

* Players alternate placing Xs and Os on the board

* The first player to reach three in a row vertically, horizontally, or diagonally wins

* If the board is filled with no winner, the game ends in a tie

* To start a new game or reset the match, select the "New Game" button

## License 

This project is MIT Licensed. The license can be viewed [here](https://bitbucket.org/dnay8979/tictactoe/src/master/LICENSE.md).

* Project can be modified and used in private
* Modifications to the project can be release under any license
* Changes made to the source code may not be documented

## Meta

Dennis Nay -- [dnay8979@conestogac.on.ca](mailto:dnay8979@conestogac.on.ca?subject=[ConestogaCollege_TicTacToe]%20Source%20Han%20Sans)

https://bitbucket.org/dnay8979/tictactoe/src/master/